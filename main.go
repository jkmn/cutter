package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {

	var limit *int = flag.Int("limit", 1000, "The limit to the number of lines you want in each file.")

	flag.Parse()

	if *limit <= 0 {
		flag.Usage()
		log.Fatal("The line limit must be a positive integer.")
	}

	args := flag.Args()
	if len(args) != 1 {
		log.Fatal("You must specificy a filename to cut.")
	}

	filename := args[0]

	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	fi, err := f.Stat()
	if err != nil {
		log.Fatal(err)
	}

	filename = fi.Name()

	split := strings.LastIndex(filename, ".")

	var format string
	if split < 0 {
		format = filename + ".%d"
	} else {
		base := filename[:split]
		ext := filename[split+1:]

		format = base + ".%d." + ext
	}

	scanner := bufio.NewScanner(f)

	var (
		outfile   *os.File
		lineCount int = 0
	)

	for scanner.Scan() {
		if lineCount%(*limit) == 0 {
			if outfile != nil {
				err = outfile.Close()
				if err != nil {
					log.Fatal(err)
				}
			}

			outfileName := fmt.Sprintf(format, lineCount/(*limit))
			outfile, err = os.Create(outfileName)
			if err != nil {
				log.Fatal(err)
			}
		}

		line := scanner.Text()
		_, err := outfile.WriteString(line + "\n")
		if err != nil {
			log.Fatal(err)
		}

		lineCount++
	}

	if outfile != nil {
		outfile.Close()
		if err != nil {
			log.Fatal(err)
		}
	}
}
